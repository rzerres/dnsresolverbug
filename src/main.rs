use std::time::Duration;

use trust_dns_resolver::TokioAsyncResolver;

#[tokio::main]
async fn main() {
    let reqwest_client_builder = reqwest::Client::builder()
        .pool_max_idle_per_host(0)
        .connect_timeout(Duration::from_secs(30))
        .timeout(Duration::from_secs(60 * 3));

    println!(
        "{:?}",
        reqwest_client_builder
            .build()
            .unwrap()
            .get(&format!(
                "https://nibiru-x.duckdns.org/.well-known/matrix/server"
            ))
            .send()
            .await
    );

    let dns_resolver = TokioAsyncResolver::tokio_from_system_conf().unwrap();
    println!(
        "{:?}",
        dns_resolver
            .srv_lookup(format!("_matrix._tcp.nibiru-x.duckdns.org."))
            .await
    );
}
